
def odd(arr):
    count = 0

    for i in range(len(arr)):
        if int(arr[i]) == 1:
            count += 1

    return count % 2


def hash_function(st):
	result = 0
	bit_odd = []

	for j in range(len(st)):

		for i in range(0, 8, 2):
			bit_odd.append(bin(int(ord(st[j])) >> 7 - i & 1)[2:])

		if odd(bit_odd) == 1:
			result = result << 1
			result += 1
			bit_odd.clear()

			if result > 65535:
				result = result & 65535
		else:
			result = result << 1
			bit_odd.clear()
			if result > 65535:
				result = result & 65535
	return result

def find_collision():
	dic = {}

	with open("words.txt", "r") as f:
		lines = f.readlines()

		for line in lines:
			dic.update([(hash_function(line.replace("\n", "")), str(line))])
	print("Количество коллизий: ", len(lines) - len(dic))

find_collision()
