#include <ctime>
#include <iostream>
#include <cassert> 
#include <string>


using namespace std;

class NDArray {
public:
    NDArray(int shape[], int fill[], int fill_size) {

        this->init_shape(shape);

        int size = this->get_size();
        array_main = new int[size];

        if (size == fill_size) {

            for (int i = 0; i < size; i++) {
                array_main[i] = fill[i];
            }
        }
        else {
            this->enter(0);
            cout << "Вводимые данные не подходят для размерности матрицы." << endl;
        }
    }

    NDArray(int shape[], int fill = -1) {

        this->init_shape(shape);

        int size = this->get_size();

        if (fill == -1) {

            array_main = new int[size];

        }
        else if (fill != -1) {

            this->enter(fill);

        }
    }

    NDArray(int shape[], string fill, int digit) {

        this->init_shape(shape);

        int size = this->get_size();
        array_main = new int[size];

        this->random(digit);

    }

    void init_shape(int shape[]) {

        shape_main = new int[2];
        for (int i = 0; i < 2; i++) {

            shape_main[i] = shape[i];

        }
    }

    void enter(int value) {

        int size = this->get_size();
        array_main = new int[size];

        for (int i = 0; i < size; i++) {

            array_main[i] = value;

        }
    }

    void random(int digit) {

        int size = this->get_size();
        array_main = new int[size];

        int dig = 10;
        while (digit != 1) {
            dig += 10;
            digit--;
        }

        for (int i = 0; i < size; i++) {

            array_main[i] = rand() % dig;

        }
    }

    int get_size() {
        return shape_main[0] * shape_main[1];
    }

    int get_element(int index) {
        return array_main[index];
    }

    int get_ID(int value) {

        int index = 0;

        while (index != shape_main[0]) {

            if (array_main[index] == value) {
                return index;
            }
            index++;
        }
        return -1;
    }

    int* get_row(int index) {

        int* result = new int[shape_main[1]];
        int elem = 0;
        int i = index * shape_main[1];

        while (elem != shape_main[1]) {

            result[elem] = array_main[i];
            i++;
            elem++;

        }

        return result;
    }

    int* get_column(int index) {

        int* result = new int[shape_main[0]];
        int elem = 0;
        int i = index;

        while (elem != shape_main[0]) {

            result[elem] = array_main[i];
            i += shape_main[1];
            elem++;

        }

        return result;
    }

    void set(int x, int value) {
        array_main[x] = value;
    }

    NDArray T() {

        int* row = new int[shape_main[1]];
        int* result = new int[this->get_size()];
        int size = this->get_size();       
        int count = 0;
        int index = 0;


        for (int i = 0; i < shape_main[0]; i++) {

            
            row = this->get_row(i);

            result[i] = row[0];
            index = i;

            while (count != shape_main[1] - 1) {

                index += shape_main[0];
                result[index] = row[count + 1];
                count++;

            }

            count = 0;

        }

        int result_shape[2] = { shape_main[1], shape_main[0] };
        NDArray out(result_shape, result, size);

        return out;

    }

    NDArray mat_mul(NDArray& other) {

        int* row = new int[shape_main[1]];
        int* cell = new int[shape_main[0]];
        int* result = new int[this->get_size()];

        int summ = 0;
        int count1 = 0;
        int count2 = 0;
        int index = 0;

        assert(shape_main[0] == other.shape_main[1] or shape_main[1] == other.shape_main[0]);           

            for (int i = 0; i < shape_main[0]; i++) {

                row = this->get_row(i);

                while (count1 != other.shape_main[1]) {

                    cell = other.get_column(count1);

                    while (count2 != shape_main[1]) {

                        summ += row[count2] * cell[count2];
                        count2++;

                    }

                    result[index] = summ;
                    summ = 0;
                    count1++;
                    count2 = 0;
                    index++;

                }

                count1 = 0;

            }
            int shape[2] = { shape_main[0], other.shape_main[1] };
            NDArray res(shape, result, shape_main[0] * other.shape_main[1]);
            return res;                 
    }   

    NDArray min(int type = -1) {

        int* row = new int[shape_main[1]];
        int* column = new int[shape_main[0]];
        
        int min = 1000000;
        int count = 0;
        int index = 0;
        if (type == -1) {

            while (count != this->get_size()) {

                if (array_main[count] < min) {
                    min = array_main[count];
                }
                count++;
            }
            int result[1] = { min };
            int res_shape[2] = { 1, 1 };
            NDArray res(res_shape, result, 1);
            return res;

        }
        else if (type == 0) {
            
            int* result = new int[shape_main[1]];

            for (int i = 0; i < shape_main[0]; i++) {

                row = this->get_row(i);

                while (count != shape_main[1]) {

                    if (row[count] < min) {
                        min = row[count];
                    }
                    count++;
                }
                result[index] = min;
                min = 1000000;
                count = 0;
                index++;

            }

            int res_shape[2] = { shape_main[0], 1 };
            NDArray res(res_shape, result, shape_main[0]);
            return res;

        }
        else if (type == 1) {

            int* result = new int[shape_main[0]];

            for (int i = 0; i < shape_main[1]; i++) {

                column = this->get_column(i);

                while (count != shape_main[0]) {

                    if (column[count] < min) {
                        min = column[count];
                    }
                    count++;
                }
                result[index] = min;
                min = 1000000;
                count = 0;
                index++;

            }

            int res_shape[2] = { 1, shape_main[1] };
            NDArray res(res_shape, result, shape_main[1]);
            return res;

        }
        
        

    }

    NDArray max(int type) {

        int* row = new int[shape_main[1]];
        int* column = new int[shape_main[0]];
        int* result = new int[shape_main[1]];
        int max = 0;
        int count = 0;
        int index = 0;

        if (type == -1) {

            while (count != this->get_size()) {

                if (array_main[count] > max) {
                    max = array_main[count];
                }
                count++;
            }
            int result[1] = { max };
            int res_shape[2] = { 1, 1 };
            NDArray res(res_shape, result, 1);
            return res;

        }

        if (type == 0) {

            for (int i = 0; i < shape_main[0]; i++) {

                row = this->get_row(i);

                while (count != shape_main[1]) {

                    if (row[count] > max) {
                        max = row[count];
                    }
                    count++;
                }
                result[index] = max;
                max = 0;
                count = 0;
                index++;

            }

            int res_shape[2] = { shape_main[0], 1 };
            NDArray res(res_shape, result, shape_main[0]);
            return res;

        }
        else if (type == 1) {

            for (int i = 0; i < shape_main[1]; i++) {

                column = this->get_column(i);

                while (count != shape_main[0]) {

                    if (column[count] > max) {
                        max = column[count];
                    }
                    count++;
                }
                result[index] = max;
                max = 0;
                count = 0;
                index++;

            }

            int res_shape[2] = { 1, shape_main[1] };
            NDArray res(res_shape, result, shape_main[1]);
            return res;

        }

    }

    NDArray mid(int type = -1) {

        int* row = new int[shape_main[1]];
        int* column = new int[shape_main[0]];
        int* result1 = new int[shape_main[1]];
        int* result2 = new int[shape_main[0]];
        int res_shape[2];
        int summ = 0;
        int count = 0;
        int index = 0;

        if (type == -1) {

            for (int i = 0; i < this->get_size(); i++) {

                summ += array_main[i];
                
            }

            int result[1] = { summ / this->get_size() };
            res_shape[0] = 1;
            res_shape[1] = 1;
            NDArray res(res_shape, result, 1);
            return res;

        }
        else if (type == 0) {

            for (int i = 0; i < shape_main[0]; i++) {

                row = this->get_row(i);

                while (count != shape_main[1]) {

                    summ += row[count];
                    count++;
                }
                result1[index] = summ / shape_main[1];
                summ = 0;
                count = 0;
                index++;
            }

            res_shape[0] = shape_main[0];
            res_shape[1] = 1;
            NDArray res(res_shape, result1, shape_main[0]);
            return res;

        }
        else if (type == 1) {
            

            for (int i = 0; i < shape_main[1]; i++) {

                column = this->get_column(i);

                while (count != shape_main[0]) {

                    summ += column[count];
                    count++;
                }
                result2[index] = summ / shape_main[0];
                summ = 0;
                count = 0;
                index++;
                //delete[]column;
            }
            res_shape[0] = 1;
            res_shape[1] = shape_main[1];
            NDArray res(res_shape, result2, shape_main[1]);
            return res;
        }

        
        

    }

    void display() {
        int indexXY = 0;
        int indexX = 0;
        int indexY = 0;

        while (indexX != shape_main[0]) {
            cout << "[";
            while (indexY != shape_main[1]) {

                cout << " " << array_main[indexXY] << " ";
                indexY++;
                indexXY++;
            }

            cout << "]" << endl;
            indexY = 0;
            indexX++;

        }
    }

    NDArray operator+(const NDArray& other) {

        const int size = this->get_size();
        int* result = new int[size];

        for (int i = 0; i < size; i++) {
            result[i] = array_main[i] + other.array_main[i];
        }
        NDArray out(shape_main, result, size);
        return out;
    }

    NDArray operator-(const NDArray& other) {

        const int size = this->get_size();
        int* result = new int[size];

        for (int i = 0; i < size; i++) {
            result[i] = array_main[i] - other.array_main[i];
        }
        NDArray out(shape_main, result, size);
        return out;
    }

    NDArray operator*(const NDArray& other) {

        const int size = this->get_size();
        int* result = new int[size];

        for (int i = 0; i < size; i++) {
            result[i] = array_main[i] * other.array_main[i];
        }
        NDArray out(shape_main, result, size);
        return out;
    }

    NDArray operator/(const NDArray& other) {

        const int size = this->get_size();
        int* result = new int[size];

        for (int i = 0; i < size; i++) {
            result[i] = array_main[i] / other.array_main[i];
        }
        NDArray out(shape_main, result, size);
        return out;
    }




private:

    int* shape_main;
    int* array_main;

};


int main() {

    setlocale(LC_ALL, "RU");
    srand(time(NULL));

    const int size = 6;
    int fill[size] = { 1,2,3,4,5,6 };
    int shape[2] = { 2, 3 };
    int shape1[2] = { 3, 2 };

    NDArray arr(shape, fill, size);
    cout << "Исходные матрицы:" << endl;
    arr.display();
    cout << endl;
    NDArray brr(shape1, "rand", 2);
    brr.display();
    cout << "=================" << endl;
    cout << "Поэлементное умножение матриц:" << endl;
    NDArray crr = arr * brr;
    crr.display();

    NDArray drr = arr.T();

    cout << "=================" << endl;
    cout << "Транспонирование:" << endl;
    drr.display();
    cout << "=================" << endl;
    cout << "Матричное умножение:" << endl;
    

    NDArray err = arr.mat_mul(drr);
    err.display();

    cout << "=================" << endl;
    cout << "Сокращение по максимальному среди строк:" << endl;
    NDArray srr = crr.max(0);
    srr.display();

    cout << "=================" << endl;
    cout << "Сокращение по минимальному среди столбцов:" << endl;

    NDArray grr = crr.min(1);
    grr.display();

    cout << "=================" << endl;
    cout << "Сокращение по среднему среди всех элементов:" << endl;

    NDArray jrr = crr.mid();
    jrr.display();
}
