
import random
import time


def find_duplicates(hash_function):

	start = time.time()
	dic = {}

	for i in range(500):

		with open("out/" + str(i) + ".txt", "r") as f:

			lines = f.readlines()
			st = ""

			for line in lines:
				s = line.split()
				for word in s:
					st += word

			dic.update([(hash_function(st), str(i))])

	print(str(hash_function) + " function execution time" + ": " + str(time.time() - start) + " s ", "Duplicate count: ", 500 - len(dic))

	return 0


def crc(line):

	h = 0

	for i in range(len(line)):

		ki = int(ord(line[i]))
		highorder = h & 0xf8000000
		h = h << 5
		h = h ^ (highorder >> 27)
		h = h ^ ki

	return h


def pjw(line):

	h = 0

	for i in range(len(line)):

		ki = int(ord(line[i]))
		h = (h << 4) + ki
		g = h & 0xf0000000
		if g != 0:
			h = h ^ (g >> 24)
			h = h ^ g

	return h


def buz(line):

	h = 0

	for i in range(len(line)):

		ki = int(ord(line[i]))
		highorder = h & 0x80000000
		h = h << 1
		h = h ^ (highorder >> 31)
		h = h ^ r(ki)

	return h


def r(char_code):

	random.seed(1)
	code = {}

	for i in range(255):
		code[i] = char_code * random.randint(0, 1000)

	return code[char_code]


find_duplicates(hash)
find_duplicates(crc)
find_duplicates(pjw)
find_duplicates(buz)
