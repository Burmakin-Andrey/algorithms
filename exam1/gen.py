
import math

maximum = math.pi * 2
minimum = -maximum
num = minimum
with open("out.dat", "w") as f:
	while num < maximum:
		num = round(num + 0.01, 2)
		f.write(str(num) + " " + str(math.sin(2 * num) + math.cos(num / 2)) + "\n")
