import matplotlib.pyplot as plt

dataX = []
dataY = []

with open("out.dat", "r") as f:
	lines = f.readlines()
	for line in lines:
		dataX.append(float(line.split()[0]))
		dataY.append(float(line.split()[1]))

plt.plot(dataX, dataY)
plt.show()
