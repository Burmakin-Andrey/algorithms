#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>
#include <fstream>
#include <math.h>
#include <iomanip>

using namespace std;

double angle(double x, double y, double z) {
	double result = ((2 * sin(x) * sin(y) + cos(z)) * 180) / M_PI;
	return result;
}

int main() {

	double x, y, z;
	int count = 0;
	ifstream file;
	file.open("out.txt");

	while (file >> x) {

		count++;
		file >> y >> z;
		cout << count << " angle: " << fixed << setprecision(6) << angle(x, y, z) << endl;

	}

	return 0;
}