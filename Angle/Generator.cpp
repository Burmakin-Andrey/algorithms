﻿#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
#include <ctime>
#include <fstream>

using namespace std;

double rand_num_PI() {

    double num = fmod(double(rand()), M_PI);
    /*double num = rand() % 2 / ((rand() + 1) % 10 * 2.0) * M_PI;*/

    if (rand() % 2 == 1) {
        return num;
    }
    else if(num != 0) {
        return -num;
    }    
    else {
        return num;
    }
}

int main(){


    srand(time(NULL));
    ofstream file;
    file.open("out.txt");

    int main_count;
    cin >> main_count;
    int count1 = 0; 
    int count2 = 0;

    while (count1 != main_count) {
        while (count2 != 3) {   
            file << rand_num_PI() << " ";
            count2++;
        }
        count2 = 0;
        file << endl;
        count1++;
    }

    file.close();   
    return 0;
}