def odd(arr):
    count = 0

    for i in range(len(arr)):
        if arr[i] == "1":
            count += 1

    return count % 2


def hash(str):
    result = []
    bit_odd = []

    for i in range(8):
        for j in range(len(str)):
            bit_odd.append(bin(ord(str[j]) >> (7 - i) & 1)[2:])
        result.append(odd(bit_odd))
        bit_odd.clear()

    return result


str = "asdqwerty"
print("Message:")
print(str)
print("Hash:")
print(hash(str))