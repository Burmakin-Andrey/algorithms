import math
def odd(arr):
    count = 0

    for i in range(len(arr)):
        if int(arr[i]) == 1:
            count += 1

    return count % 2


def hash(str):

    result = []
    bit_odd = []

    for i in range(8):
        for j in range(4):
            bit_odd.append(bin(int(str[j]) >> (7 - i) & 1)[2:])
        result.append(odd(bit_odd))
        bit_odd.clear()

    if odd(result) == int(str[4]):
        return 0
    else:
        return 1

with open("out.txt", "r") as f:
    count = 0
    for i in range(100):
        count += hash(f.readline().split())

    print(float(count) / 100 * 100, "%")